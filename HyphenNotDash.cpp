#include "HyphenNotDash.h"

void HyphenNotDash::interpret(std::string& text) {
    std::regex hyphen_to_dash("( - )");
    text = std::regex_replace(text, hyphen_to_dash, " — ");
}
