#pragma once
#include "AbstractTextFormatter.h"
class IncorrectTabs : public AbstractTextFormatter {
    public:
        void interpret(std::string& text);
};
