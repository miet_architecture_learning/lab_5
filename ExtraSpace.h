#pragma once
#include "AbstractTextFormatter.h"
class ExtraSpace : public AbstractTextFormatter {
    public:
        void interpret(std::string& text);
};
