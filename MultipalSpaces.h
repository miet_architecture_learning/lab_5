#pragma once
#include "AbstractTextFormatter.h"
class MultipalSpaces : public AbstractTextFormatter {
    public:
        void interpret(std::string& text);
};
