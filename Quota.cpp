#include "Quota.h"

void Quota::interpret(std::string& text) {
    std::regex left_quota("“");
    std::regex right_quota("”");
    text = std::regex_replace(text, left_quota, "«");
    text = std::regex_replace(text, right_quota, "»");
}
