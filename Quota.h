#pragma once
#include "AbstractTextFormatter.h"
class Quota : public AbstractTextFormatter {
    public:
        void interpret(std::string& text);
};
