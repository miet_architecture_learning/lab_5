#include "MultipalSpaces.h"
#include "HyphenNotDash.h"
#include "Quota.h"
#include "IncorrectTabs.h"
#include "ExtraSpace.h"
#include "MultipalNewLines.h"
#include <ios>
#include <iostream>
#include <fstream>
#include <string>
int main() {
    std::string filename = "examples/text.txt";
    std::fstream text_file{filename, std::ios::binary | std::ios::in};
    if (!text_file.is_open()) {
        std::cout << "Failed to open: " << filename << '\n';
    }
    std::string text = "";
    std::string templine;
    while (text_file) {
        std::getline(text_file, templine);
        text += templine + "\n";
    }

    std::cout << "Исходный текст:\n"<< text << "------- Конец исходного текста ---------\n";

    MultipalSpaces multipal_spaces;
    HyphenNotDash hyphen_not_dash;
    Quota quota;
    IncorrectTabs incorrect_tabs; 
    ExtraSpace extra_space;
    MultipalNewLines mult_new_lines;

    multipal_spaces.interpret(text);
    hyphen_not_dash.interpret(text);
    quota.interpret(text);
    incorrect_tabs.interpret(text);
    extra_space.interpret(text);
    mult_new_lines.interpret(text);

    std::cout << "Измененный текст:\n"<< text << "------- Конец измененного текста ---------\n";
}
