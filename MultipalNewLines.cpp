#include "MultipalNewLines.h"

void MultipalNewLines::interpret(std::string& text) {
    std::regex mult_new_lines("\n+");
    text = std::regex_replace(text, mult_new_lines, "\n");
}
