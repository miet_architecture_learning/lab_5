#pragma once
#include "AbstractTextFormatter.h"
class MultipalNewLines : public AbstractTextFormatter {
    public:
        void interpret(std::string& text);
};
