#include "IncorrectTabs.h"

void IncorrectTabs::interpret(std::string& text) {
    std::regex incorrect_tabs("\t+");
    text = std::regex_replace(text, incorrect_tabs, "\t");
}
