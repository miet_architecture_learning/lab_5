#pragma once
#include "AbstractTextFormatter.h"
class HyphenNotDash : public AbstractTextFormatter {
    public:
        void interpret(std::string& text);
};
