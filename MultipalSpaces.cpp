#include "MultipalSpaces.h"

void MultipalSpaces::interpret(std::string& text) {
    std::regex mult_spaces_regex("[ ]+");
    text = std::regex_replace(text, mult_spaces_regex, " ");
}

