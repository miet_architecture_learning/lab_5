#include "ExtraSpace.h"

void ExtraSpace::interpret(std::string& text) {
    std::regex extra_space_after_open_parenthesis("\\( +");
    std::regex extra_space_before_close_parenthesis(" +\\)");
    std::regex extra_space_before_comma(" +\\,");
    std::regex extra_space_before_dot(" +\\.");
    text = std::regex_replace(text, extra_space_after_open_parenthesis, "(");
    text = std::regex_replace(text, extra_space_before_close_parenthesis, ")");
    text = std::regex_replace(text, extra_space_before_comma, ",");
    text = std::regex_replace(text, extra_space_before_dot, ".");
}
