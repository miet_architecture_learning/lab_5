#pragma once
#include <regex>
#include <string>
class AbstractTextFormatter {
    protected:
        virtual void interpret(std::string& text) = 0;
    public:
        virtual ~AbstractTextFormatter();
};
